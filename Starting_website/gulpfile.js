const gulp = require('gulp');
const htmlmin = require('gulp-htmlmin');
const cleanCSS = require('gulp-clean-css');
const uglify = require('gulp-uglify');
const imagemin = require('gulp-imagemin');
const pipeline = require('readable-stream').pipeline;

// Gulp task to minify HTML files
gulp.task('minify-html', function() {
  return gulp.src(['*.html'])
    .pipe(htmlmin({
      collapseWhitespace: true,
      removeComments: true
    }))
    .pipe(gulp.dest('./public'));
});

// Gulp task to minify JavaScript files
gulp.task('minify-js', function() {
  return gulp.src(['./js/*.js'])
    .pipe(uglify())
    .pipe(gulp.dest('./public/js'));
});

// Gulp task to minify Css files

gulp.task('minify-css', function() {
  return gulp.src(['./css/*.css'])
    .pipe(cleanCSS())
    .pipe(gulp.dest('./public/css'));
});

//Gulp image-min

 
gulp.task ('compress-img', function() {
    return gulp.src('./img/*')
        .pipe(imagemin())
        .pipe(gulp.dest('public/img'));
});

// ALL MOTHERFUCKER

gulp.task('be-minified', gulp.series('minify-html', 'minify-js', 'minify-css', 'compress-img'))
